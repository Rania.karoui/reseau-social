import React, { useContext } from "react";

import { Route, Redirect } from "react-router-dom";
import Register from "../pages/Register";
import Login from "../pages/Login";
import UserContext from "../contexts/UserContext";

const AuthRoutes = () => {
  const { isLoggedIn } = useContext(UserContext);

  return [
    <Route
      exact
      path="/login"
      render={({ location }) =>
        !isLoggedIn ? (
          <Login />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location },
            }}
          />
        )
      }
    />,
    <Route exact path="/register" render={() => <Register />} />,
  ];
};

export default AuthRoutes;
