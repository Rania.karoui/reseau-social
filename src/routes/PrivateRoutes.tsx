import React from "react";

import { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import UserContext from "../contexts/UserContext";
import Home from "../pages/Home";
import Profile from "../pages/Profile";
import AddFeed from "../pages/AddFeed";

const PrivateRoutes = () => {
  const { isLoggedIn } = useContext(UserContext);
  return [
    <Route
      exact
      path="/"
      render={({ location }) =>
        isLoggedIn ? (
          <Home />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />,
    <Route
      exact
      path="/profile"
      render={({ location }) =>
        isLoggedIn ? (
          <Profile />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />,
    <Route
      exact
      path="/add-feed"
      render={({ location }) =>
        isLoggedIn ? (
          <AddFeed />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />,
  ];
};

export default PrivateRoutes;
