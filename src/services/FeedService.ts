import axios from "axios";
import { API_URL } from "../constants/Constatns";
import { Feed } from "../models/Feed";

export const getFeeds = async (accessToken: string | null) => {
  let result: Feed[] = [];
  try {
    const url = `${API_URL}/posts`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    const response = await axios.get(url, { headers });
    const formattedResult = response.data.map((feed: Feed) => {
      const { id, title, content } = feed;
      return { id, title, content };
    });
    result = formattedResult;
  } catch (e) {
    console.log(e);
  }

  return result;
};

export const addFeed = async (
  feed: Feed,
  user: string,
  accessToken: string | null
) => {
  let result = null;
  const { title, content } = feed;
  const body = { title, content, user };

  try {
    const url = `${API_URL}/posts`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    await axios.post(url, body, { headers });
  } catch (e) {
    throw e;
  }

  return result;
};
