import React, { useContext, useState, useEffect } from "react";
import Button from "../components/Button";
import UserContext from "../contexts/UserContext";
import Input from "../components/Input";
import { useHistory } from "react-router-dom";
import Checkbox from "../components/Checkbox";

const Login = () => {
  const { actions } = useContext(UserContext);
  const history = useHistory();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const [isRememberChecked, setIsRememberChecked] = useState<boolean>(false);

  const _getUser = () => {
    let parsedUser = null;
    const user = localStorage.getItem("user");
    if (user) {
      parsedUser = JSON.parse(user);
    }
    return parsedUser;
  };

  const _initState = () => {
    const user = _getUser();
    if (user) {
      setUsername(user.username);
      setIsRememberChecked(true);
    }
  };

  useEffect(() => {
    _initState();
  }, []);

  const _signIn = () => actions?.signIn(username, password, isRememberChecked);

  const _goToRegisterPage = () => history.push("register");

  return (
    <div className="LoginContainer">
      <div className="Login">
        <h1>S'authentifier</h1>
        <Input
          placeholder="Nom d'utilisateur"
          value={username}
          onChange={(event: any) => setUsername(event.target.value)}
        />
        <Input
          placeholder="Mot de passe"
          value={password}
          type="password"
          onChange={(event: any) => setPassword(event.target.value)}
        />
        <Button primary onClick={_signIn}>
          Login
        </Button>
        <Checkbox
          label="Se souvenir de moi ?"
          checked={isRememberChecked}
          onClick={() => setIsRememberChecked(!isRememberChecked)}
        />
        <Button onClick={_goToRegisterPage}>Register</Button>
      </div>
    </div>
  );
};

export default Login;
