import { RegisterForm } from "./RegisterForm";

export interface UserActions {
  signIn: (username: string, password: string, remember: boolean) => void;
  signOut: () => void;
  register: (form: RegisterForm) => void;
}
