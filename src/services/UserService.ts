import axios from "axios";
import { API_URL } from "../constants/Constatns";
import { User } from "../models/User";

export const getUsers = async (accessToken: string | null) => {
  let result: User[] = [];
  try {
    const url = `${API_URL}/users`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    const response = await axios.get(url, { headers });
    const formattedResult = response.data.map((user: User) => {
      const { id, username, email, lastname, firstname } = user;
      return { id, username, email, lastname, firstname };
    });
    result = formattedResult;
  } catch (e) {
    console.log(e);
  }

  return result;
};

export const getAuthentifiedUser = async (accessToken: string) => {
  let result: User | null = null;
  try {
    const url = `${API_URL}/users/me`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    const response = await axios.get(url, { headers });
    const { id, username, email, lastname, firstname } = response.data;

    result = {
      id,
      username,
      email,
      lastname,
      firstname,
    };
  } catch (e) {
    console.log(e);
  }

  return result;
};

export const updateUser = async (user: User, accessToken: string) => {
  let result = null;
  const { id, username, email, firstname, lastname, password } = user;
  const body = { username, email, firstname, lastname, password };

  try {
    const url = `${API_URL}/users/${id}`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    await axios.put(url, body, { headers });
  } catch (e) {
    throw e;
  }

  return result;
};

export const deleteUser = async (id: number, accessToken: string) => {
  let result = null;

  try {
    const url = `${API_URL}/users/${id}`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    };

    const reponse = await axios.delete(url, { headers });
    result = reponse;
  } catch (e) {
    throw e;
  }

  return result;
};
