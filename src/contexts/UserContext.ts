import React from "react";
import { UserStore } from "../models/UserStore";

const UserContext = React.createContext<UserStore>({
  user: null,
  isLoggedIn: false,
  accessToken: null,
});

export default UserContext;
