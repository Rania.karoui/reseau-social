export interface RegisterForm {
  username: string;
  email: string;
  password: string;
  firstname: string;
  lastname: string;
}
