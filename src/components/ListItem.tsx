import React from "react";
interface IProps {
  primaryText: string;
  secondaryText: string[];
}

export const ListItem: React.FC<IProps> = ({ primaryText, secondaryText }) => (
  <div>
    <h2>{primaryText}</h2>
    {secondaryText.map((text) => (
      <div>{text}</div>
    ))}
    <hr />
  </div>
);
