import React, { useState, useContext } from "react";
import Input from "../components/Input";
import Button from "../components/Button";
import UserContext from "../contexts/UserContext";
import { addFeed } from "../services/FeedService";
import { useHistory } from "react-router-dom";

const AddFeed = () => {
  const history = useHistory();
  const { user, accessToken } = useContext(UserContext);

  const [title, setTitle] = useState<string>("");
  const [content, setContent] = useState<string>("");

  const _addFeed = async () => {
    try {
      await addFeed({ title, content }, String(user?.id), accessToken);
      history.push("/");
    } catch (e) {
      alert(e);
    }
  };

  return (
    <div className="RegisterContainer">
      <div className="Register">
        <h1>Post</h1>

        <Input
          placeholder="Titre"
          value={title}
          onChange={(event: any) => setTitle(event.target.value)}
        />
        <Input
          placeholder="Contenu"
          value={content}
          onChange={(event: any) => setContent(event.target.value)}
        />
        <Button onClick={_addFeed}>Confirmer</Button>
      </div>
    </div>
  );
};

export default AddFeed;
