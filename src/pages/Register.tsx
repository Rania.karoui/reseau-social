import React, { useState, useContext } from "react";
import Input from "../components/Input";
import Button from "../components/Button";
import { RegisterForm } from "../models/RegisterForm";
import UserContext from "../contexts/UserContext";

const Register = () => {
  const { actions } = useContext(UserContext);

  const [form, setForm] = useState<RegisterForm>({
    firstname: "",
    lastname: "",
    email: "",
    username: "",
    password: "",
  });

  const _register = () => actions?.register(form);

  return (
    <div className="RegisterContainer">
      <div className="Register">
        <h1>S'enregistrer</h1>

        <Input
          placeholder="Nom"
          value={form.lastname}
          onChange={(event: any) =>
            setForm({ ...form, lastname: event.target.value })
          }
        />
        <Input
          placeholder="Prénom"
          value={form.firstname}
          onChange={(event: any) =>
            setForm({ ...form, firstname: event.target.value })
          }
        />
        <Input
          placeholder="Email"
          value={form.email}
          onChange={(event: any) =>
            setForm({ ...form, email: event.target.value })
          }
        />
        <Input
          placeholder="Nom d'utilisateur"
          value={form.username}
          onChange={(event: any) =>
            setForm({ ...form, username: event.target.value })
          }
        />
        <Input
          placeholder="Mot de passe"
          value={form.password}
          type="password"
          onChange={(event: any) =>
            setForm({ ...form, password: event.target.value })
          }
        />
        <Button onClick={_register}>Confirmer</Button>
      </div>
    </div>
  );
};

export default Register;
