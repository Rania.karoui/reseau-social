import axios from "axios";
import { API_URL } from "../constants/Constatns";
import { RegisterForm } from "../models/RegisterForm";

export const signIn = async (identifier: string, password: string) => {
  let result = null;
  try {
    const url = `${API_URL}/auth/local`;
    const data = {
      identifier,
      password,
    };
    const headers = {
      "Content-Type": "application/json",
    };

    const response = await axios.post(url, data, { headers });
    const { jwt, user } = response.data;
    const { id, username, email, lastname, firstname } = user;

    result = {
      accessToken: jwt,
      user: { id, username, email, firstname, lastname },
    };
  } catch (e) {
    console.log(e);
  }

  return result;
};

export const register = async (form: RegisterForm) => {
  let result = null;
  try {
    const url = `${API_URL}/auth/local/register`;
    const headers = {
      "Content-Type": "application/json",
    };

    const response = await axios.post(url, form, { headers });
    const { jwt, user } = response.data;
    const { id, username, email, lastname, firstname } = user;

    result = {
      accessToken: jwt,
      user: { id, username, email, firstname, lastname },
    };
  } catch (e) {
    console.log(e);
  }

  return result;
};
